const playlistController = {};

const Playlist = require('../models/Playlist.js');

playlistController.getPlaylists = async function(ctx, next) {
  ctx.body = await Playlist.find().sort({ creationDate: -1 });
};

playlistController.getPlaylist = async function(ctx, next) {
  ctx.body = await Playlist.findOne({ _id: ctx.params.id });
};

playlistController.getVideosInPlaylist = async function(ctx, next) {
  ctx.body = await Playlist.findOne({ _id: ctx.params.id }, 'videos');
};

playlistController.addPlaylist = async function(ctx, next) {
  const playlist = new Playlist(ctx.request.body);
  const res = await playlist.save();
  if (!res) console.error('error saving playlist');
  else {
      console.log('Playlist saved: ', res);
      ctx.body = res;
  }
};

playlistController.addVideoToPlaylist = async function(ctx, next) {
  ctx.body = await Playlist.findOne({ _id: ctx.params.id }, (err, playlist) => {
    if (err) return console.error(err);
    playlist.videos.push(ctx.request.body);
    playlist.save((err, pl) => {
      if (err) return console.log(err);
      console.log('playlist updated', pl);
    });
  });
};

module.exports = playlistController;
