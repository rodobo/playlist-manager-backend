const mongoose = require('mongoose');

const playlistSchema = mongoose.Schema({
  createdBy: String,
  creationDate: { type: Date, default: Date.now },
  name: String,
  description: String,
  videos: [{
    addedBy: String,
    dateAdded: { type: Date, default: Date.now },
    youtube_id: String,
    description: String,
  }],
});

module.exports = mongoose.model('Playlist', playlistSchema);
