const hostname = '127.0.0.1';
const port = 8080;

const koa = require('koa');
const app = new koa();
const bodyParser = require('koa-bodyparser')();
const cors = require('koa-cors');
const router = require('./router.js');
var db = require('./db.js');

app.use(cors());
app.use(bodyParser);
app.use(router.routes());

app.listen(port, hostname, () => {
  console.log(`Server running at http://${hostname}:${port}/`);
});
