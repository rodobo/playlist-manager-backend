const koa = require('koa');
const router = require('koa-router')();
const playlistController = require('./controllers/playlistController');

router.use(router.routes());

router.get('/', ctx => {
  ctx.body = 'Welcome to the Playlist Manager API';
  ctx.status = 200;
});

router.get('/playlist', playlistController.getPlaylists);

router.post('/playlist', playlistController.addPlaylist);

router.get('/playlist/:id', playlistController.getPlaylist);

router.post('/playlist/:id', playlistController.addVideoToPlaylist);

router.get('/playlist/:id/videos', playlistController.getVideosInPlaylist);

module.exports = router;
