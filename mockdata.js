#!/usr/local/bin/node

const db = require('./db.js');
const Playlist = require('./models/playlist.js');

Playlist.create({
  createdBy: "Joe Doe",
  name: "Rock Music Videos",
  description: "Videos that me wanna rock!",
  videos: [{
    addedBy: "Joe Cool",
    youtube_id: "zRIbf6JqkNc",
    description: "Music video by Guns N' Roses performing Don't Cry (Original Version). (C) 1991 Guns N' Roses under exclusive license to Geffen Records",
  },
  {
    addedBy: "John Doe",
    youtube_id: "NMNgbISmF4I",
    description: "Music video by Aerosmith performing Crazy. (C) 1994 Geffen Records",
  },
  {
    addedBy: "Jane Doe",
    youtube_id: "9BMwcO6_hyA",
    description: "Music video by Bon Jovi performing Always. (C) 1994 The Island Def Jam Music Group",
  }],
});
