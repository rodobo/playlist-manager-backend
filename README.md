# Playlist Manager backend

### Description

An API to manage data for the [Playlist Manager](https://bitbucket.org/rodobo/playlist-manager)

### Installation

* Clone this repo.
* In the project folder, run `npm install` or `yarn` to install all project dependencies.
* Ensure you have a MongoDB server running.
* Update the db connection string in the `db.js` file.
* To launch the node server and connect to the db server, run `node index.js`.

### API

#### Create Playlist : `[POST] /playlist`

Sample Request:
```JSON
{
    "createdBy": "Jane Doe",
    "name": "Cooking Videos",
    "description": "Features recipes for popular Spanish dishes"
}
```

Sample Response:
```JSON
{
    "_id": "5adf3255918cfae6978cd790",
    "createdBy": "Jane Doe",
    "name": "Cooking Videos",
    "description": "Features recipes for popular Spanish dishes",
    "creationDate": "2018-04-24T13:34:13.106Z",
    "videos": [],
    "__v": 0
}
```

#### List all playlists : `[GET] /playlist`

Sample Response:
```JSON
[
  {
      "creationDate": "2018-04-24T00:51:07.654Z",
      "videos": [],
      "_id": "5ade7f7bcc1871d54d6ddc1d",
      "createdBy": "John Doe",
      "name": "Rock Music Videos",
      "description": "Videos that me wanna rock!",
      "__v": 0
  },
  {
      "creationDate": "2018-04-24T00:53:17.059Z",
      "videos": [
          {
              "dateAdded": "2018-04-24T00:53:17.058Z",
              "_id": "5ade7ffd5f87a1d570c77617",
              "addedBy": "Jane Doe",
              "youtube_id": "9BMwcO6_hyA",
              "description": "Music video by Bon Jovi performing Always. (C) 1994 The Island Def Jam Music Group"
          },
          {
              "dateAdded": "2018-04-24T02:32:52.617Z",
              "_id": "5ade9754c9d4fed9663d169e",
              "addedBy": "Ray Ban",
              "youtube_id": "jqJj8sU-0yo",
              "description": "Concierto en el \"Teatro Circo Price\" de Madrid el 6 de Noviembre de 2010"
          }
      ],
      "_id": "5ade7ffd5f87a1d570c77616",
      "createdBy": "Joe Doe",
      "name": "Rock Music Videos 2",
      "description": "More videos that me wanna rock!",
      "__v": 1
  }
]
```

#### Get info for a single playlist: `[GET] /playlist/{id}`

Sample Response:
```JSON
{
    "creationDate": "2018-04-24T00:53:17.059Z",
    "videos": [
        {
            "dateAdded": "2018-04-24T00:53:17.058Z",
            "_id": "5ade7ffd5f87a1d570c77617",
            "addedBy": "Jane Doe",
            "youtube_id": "9BMwcO6_hyA",
            "description": "Music video by Bon Jovi performing Always. (C) 1994 The Island Def Jam Music Group"
        },
        {
            "dateAdded": "2018-04-24T02:32:52.617Z",
            "_id": "5ade9754c9d4fed9663d169e",
            "addedBy": "Ray Ban",
            "youtube_id": "jqJj8sU-0yo",
            "description": "Concierto en el \"Teatro Circo Price\" de Madrid el 6 de Noviembre de 2010"
        }
    ],
    "_id": "5ade7ffd5f87a1d570c77616",
    "createdBy": "Joe Doe",
    "name": "Rock Music Videos 2",
    "description": "More videos that me wanna rock!",
    "__v": 1
}
```

#### Add video to playlist : `[POST] /playlist/{id}`

Sample Request:
```JSON
{
            "addedBy": "George",
            "youtube_id": "1111111",
            "description": "Popular Beatles Video"
}
```

Sample Response: (*Response provides playlist document before update*)
```JSON
{
    "creationDate": "2018-04-24T00:53:17.059Z",
    "videos": [
        {
            "dateAdded": "2018-04-24T00:53:17.058Z",
            "_id": "5ade7ffd5f87a1d570c77617",
            "addedBy": "Jane Doe",
            "youtube_id": "9BMwcO6_hyA",
            "description": "Music video by Bon Jovi performing Always. (C) 1994 The Island Def Jam Music Group"
        },
        {
            "dateAdded": "2018-04-24T02:32:52.617Z",
            "_id": "5ade9754c9d4fed9663d169e",
            "addedBy": "Ray Ban",
            "youtube_id": "jqJj8sU-0yo",
            "description": "Concierto en el \"Teatro Circo Price\" de Madrid el 6 de Noviembre de 2010"
        }
    ],
    "_id": "5ade7ffd5f87a1d570c77616",
    "createdBy": "Joe Doe",
    "name": "Rock Music Videos 2",
    "description": "More videos that me wanna rock!",
    "__v": 1
}
```

#### List all videos for a playlist : `[GET] /playlist/{id}/videos`

Sample Response:
```JSON
{
    "videos": [
        {
            "dateAdded": "2018-04-24T00:53:17.058Z",
            "_id": "5ade7ffd5f87a1d570c77619",
            "addedBy": "Joe Cool",
            "youtube_id": "zRIbf6JqkNc",
            "description": "Music video by Guns N' Roses performing Don't Cry (Original Version). (C) 1991 Guns N' Roses under exclusive license to Geffen Records"
        },
        {
            "dateAdded": "2018-04-24T00:53:17.058Z",
            "_id": "5ade7ffd5f87a1d570c77618",
            "addedBy": "John Doe",
            "youtube_id": "NMNgbISmF4I",
            "description": "Music video by Aerosmith performing Crazy. (C) 1994 Geffen Records"
        },
        {
            "dateAdded": "2018-04-24T00:53:17.058Z",
            "_id": "5ade7ffd5f87a1d570c77617",
            "addedBy": "Jane Doe",
            "youtube_id": "9BMwcO6_hyA",
            "description": "Music video by Bon Jovi performing Always. (C) 1994 The Island Def Jam Music Group"
        }
    ],
    "_id": "5ade7ffd5f87a1d570c77616"
}
```


### Tech Stack:
- [Node.js](https://nodejs.org)
- [KoaJS](http://koajs.com)
- [MongoDB](https://www.mongodb.com)

### Package Dependencies:
- [Mongoose](https://github.com/)
- [koa-router](https://github.com/alexmingoia/koa-router)
- [koa-bodyparser](https://github.com/koajs/bodyparser)
- [koa-cors](https://github.com/koajs/cors)

### License

Submit any bugs by creating an issue or submitting a PR.
This repo is published under the MIT License.
